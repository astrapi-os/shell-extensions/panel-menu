## Your own menu for the Gnome Panel

### Description
Inspired by SSHMenu and Command Menu, this extension offers the possibility to add several custom menus to the Gnome Panel.

### Installation
#### One-click install
<a href="https://extensions.gnome.org/extension/6877/astrapios-panel-menu/">
<img src="https://github.com/andyholmes/gnome-shell-extensions-badge/raw/master/get-it-on-ego.svg" alt="Get it on EGO" width="200" />
</a>

#### Manual install
To install, simply download as zip and unzip contents in ~/.local/share/gnome-shell/extensions/panel-menu@astrapi.os

### Supports

* Multiple menus 
* Submenus ( versions >= 4 )
* Reload button ( versions >= 4 )
* Seperator ( versions >= 4 )

### example1.json

![screenshot](<example1.png>)

### example2.json

![screenshot](<example2.png>)

### example3.json

![screenshot](<example3.png>)

### Icons

Browse icons at: https://iconduck.com/sets/adwaita-icon-theme
